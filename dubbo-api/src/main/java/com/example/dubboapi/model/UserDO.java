package com.example.dubboapi.model;

import lombok.Data;
import java.io.Serializable;

/**
 * @Author:wangc
 * @Date: 2020/7/1
 * @Time: 10:39
 */

@Data
public class UserDO implements Serializable{
    private Integer id;
    private String userName;
    private String passWord;
    private String realName;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", realName='" + realName + '\'' +
                '}';
    }
}
