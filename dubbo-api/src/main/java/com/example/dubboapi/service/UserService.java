package com.example.dubboapi.service;

import com.example.dubboapi.model.UserDO;
import com.example.dubboapi.util.PageRequest;
import com.example.dubboapi.util.PageResult;

import java.util.List;

/**
 * @Author:wangc
 * @Date: 2020/7/1
 * @Time: 10:39
 */

public interface UserService {

    /**
     * 通过id查找
     * @param id
     * @return
     */
    public UserDO getById(int id);

    /**
     * 通过userName查找
     * @param userName,pageNum,pageSize
     */
    public List<UserDO> listByUserName(String userName,int pageNum,int pageSize);

    /**
     * 通过realName查找
     * @param realName,pageNum,pageSize
     */
    public List<UserDO> listByRealName(String realName,int pageNum,int pageSize);

    /*
     *分页查询
     *@param pageRequest
     *@return
     */
    public PageResult listByPage(PageRequest pageRequest);
}
