package com.example.dubboapi.util;

/**
 * @Author:wangc
 * @Date: 2020/7/1
 * @Time: 10:39
 */

import lombok.Data;

import java.io.Serializable;

/**
 * 分页请求
 */
@Data
public class PageRequest implements Serializable {
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页数量
     */
    private int pageSize;

}

