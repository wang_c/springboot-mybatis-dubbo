package com.example.dubboprovider.mapper;

import com.example.dubboapi.model.UserDO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {

    //通过id查询
    UserDO getById(int id);

    List<UserDO> listByUserName(String userName);

    /**
     * 通过realName模糊查询
     */
    List<UserDO> listByRealName(String realName);

    /**
     * 分页查询
     */
    List<UserDO> selectPage();

}