package com.example.dubboprovider.impl;

import com.example.dubboapi.model.UserDO;
import com.example.dubboapi.service.UserService;
import com.alibaba.dubbo.config.annotation.Service;
import com.example.dubboapi.util.PageRequest;
import com.example.dubboapi.util.PageResult;
import com.example.dubboapi.util.PageUtils;
import com.example.dubboprovider.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author:wangc
 * @Date: 2020/7/1
 * @Time: 10:39
 */

@Service(
        version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}"

)

public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public UserDO getById(int id) {
        return userMapper.getById(id);
    }

    @Override
    public List<UserDO> listByUserName(String userName,int pageNum,int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.listByUserName(userName);
    }

    @Override
    public List<UserDO> listByRealName(String realName,int pageNum,int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.listByUserName(realName);
    }

    @Override
    public PageResult listByPage(PageRequest pageRequest){

        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    /**
     * 调用分页插件完成分页
     * @param pageRequest
     * @return
     */
    private PageInfo<UserDO> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<UserDO> Menus = userMapper.selectPage();
        return new PageInfo<UserDO>(Menus);
    }
}
