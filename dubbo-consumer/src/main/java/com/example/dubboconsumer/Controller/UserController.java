package com.example.dubboconsumer.Controller;

import com.example.dubboapi.service.UserService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.example.dubboapi.util.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Reference(
            version = "1.0.0",
            application = "${dubbo.application.id}",
            url = "dubbo://localhost:12345"
    )
    UserService userService;

    @GetMapping("/getUserById/{id}")
    public String findById(@PathVariable int id) {
        return userService.getById(id).toString();
    }
    //通过userName查询，在url中输入参数
    @RequestMapping(value = "/listUserByUserName/{userName}/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object GetUserByUserName(@PathVariable String userName,@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){
        return userService.listByUserName(userName,pageNum,pageSize);
    }

    //通过realName查询，在url中输入参数
    @RequestMapping(value = "/listUserByRealName/{realName}/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object GetUserByRealName(@PathVariable String realName,@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){
        return userService.listByRealName(realName,pageNum,pageSize);
    }

    //分页查询
    @PostMapping(value="/listByPage")
    public Object listByPage(@RequestBody PageRequest pageQuery) {
        return userService.listByPage(pageQuery);
    }
}
